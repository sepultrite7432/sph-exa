#pragma once

namespace sphexa
{

int initAndGetRankId()
{
    int rank = 0;
#ifdef USE_MPI
		/*================================================================*/
    /* Since the MPI processes 'can' be multi-threaded, MPI_Init(...) */
    /* should be replaced by MPI_Init_thread(...)											*/
		/*================================================================*/
		int provided, required = MPI_THREAD_FUNNELED; 
		MPI_Comm start_comm = MPI_COMM_WORLD; 
    MPI_Init_thread(NULL, NULL, required, &provided);
    if(provided != required)
    	exit(1);
    	 
    MPI_Comm_rank(start_comm, &rank);
    if (rank == 0) {
        int mpi_version, mpi_subversion, mpi_ranks;
        MPI_Get_version(&mpi_version, &mpi_subversion);
        MPI_Comm_size(start_comm, &mpi_ranks);
#ifdef _OPENMP
        printf("# %d MPI-%d.%d process(es) with %d OpenMP-%u thread(s)/process\n",
        mpi_ranks, mpi_version, mpi_subversion, omp_get_max_threads(), _OPENMP);
#else
        printf("# %d MPI-%d.%d process(es) with 1 thread/process\n",
        mpi_ranks, mpi_version, mpi_subversion);			
        //Removed the word "OpenMP" from printf stmt. as not compiled with OpenMP support
#endif
    }
#endif
    return rank;
}

int exitSuccess()
{
#ifdef USE_MPI
    MPI_Finalize();
#endif
    return EXIT_SUCCESS;
}

} // namespace sphexa
